(function(){ 
'use strict';

angular.module('<%= slugifiedModuleName %>').controller('<%= classifiedControllerName %>Controller', <%= classifiedControllerName %>);
	/* @ngInject */
	function <%= classifiedControllerName %>() {
		var self=this; // jshint ignore:line
		// <%= humanizedControllerName %> controller logic
		// ...
	}
	
})();