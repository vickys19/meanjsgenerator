(function(){ 
'use strict';

angular.module('<%= slugifiedModuleName %>').factory('<%= classifiedName %>', '<%= classifiedName %>');
	/* @ngInject */
	function '<%= classifiedName %>'() {
		// <%= humanizedName %> service logic
		// ...

		var self=this; // jshint ignore:line	
		var service={
			someMethod:someMethod
		}

		function someMethod(){
			return true;
		}

		return service;
	}

})();