'use strict';

// Use application configuration module to register a new module
appConfigFactory.registerModule('<%= slugifiedName %>');
