'use strict';


// Init the application configuration module for AngularJS application
var appConfigFactory = (function() {

	// Init module configuration options
	var appModule = '<%= slugifiedAppName %>';
	var appModuleVendorDependencies = ['ngResource',<% if (angularCookies) { %> 'ngCookies', <% } if (angularAnimate) { %> 'ngAnimate', <% } if (angularTouch) { %> 'ngTouch', <% } if (angularSanitize) { %> 'ngSanitize', <% } %> 'ui.router', 'ui.bootstrap', 'ui.utils'];

	// Add a new module
	var registerModule = function(moduleName, dependencies) {
		// Create angular module
		angular.module(moduleName, dependencies || []);

		// Add the module to the AngularJS configuration file
		angular.module(appModule).requires.push(moduleName);
	};

	return {
		appModule: appModule,
		appModuleVendorDependencies: appModuleVendorDependencies,
		registerModule: registerModule
	};
})();