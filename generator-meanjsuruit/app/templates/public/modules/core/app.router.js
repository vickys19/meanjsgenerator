(function () {

'use strict';

// Setting up route
angular.module('core').config(

	function($stateProvider, $urlRouterProvider) {
		// Redirect to home view when route not found
		$urlRouterProvider.otherwise('/');

		// Home state routing
		$stateProvider.
		state('home', {
			url: '/',
			views: {
                    'header': {
                        templateUrl: 'modules/core/views/headerTemplate.html'
                    },
                    'body': {
                        templateUrl: 'modules/core/views/homeTemplate.html'
                    }
                }
		});
	}
);

})();