(function () {

'use strict';

//Menu service used for managing  menus
angular.module('core').factory('Menus',menuServices);
	
    /* @ngInject */
	function menuServices($q) {

		var service={
			shouldRender:shouldRender,
			validateMenuExistance:validateMenuExistance,
			getMenu:getMenu,
			addMenu:addMenu,
			removeMenu:removeMenu,
			addMenuItem:addMenuItem,
			addSubMenuItem:addSubMenuItem,
			removeMenuItem:removeMenuItem,
			removeSubMenuItem:removeSubMenuItem
		};

		var self=this; // jshint ignore:line

		// Define a set of default roles
		self.defaultRoles = ['*'];

		// Define the menus object
		self.menus = {};

		// A private function for rendering decision 
		function shouldRender(user) {
			if (user) {
				if (!!~this.roles.indexOf('*')) {// jshint ignore:line
					return true;
				} else {
					for (var userRoleIndex in user.roles) {
						for (var roleIndex in this.roles) {// jshint ignore:line
							if (this.roles[roleIndex] === user.roles[userRoleIndex]) {// jshint ignore:line
								return true;
							}
						}
					}
				}
			} else {
				return this.isPublic;// jshint ignore:line
			}

			return false;
		}

		// Validate menu existance
		function validateMenuExistance (menuId) {
			if (menuId && menuId.length) {
				if (self.menus[menuId]) {
					return true;
				} else {
					throw new Error('Menu does not exists');
				}
			} else {
				throw new Error('MenuId was not provided');
			}

			return false;
		}

		// Get the menu object by menu id
		function getMenu(menuId) {
			// Validate that the menu exists
			validateMenuExistance(menuId);

			// Return the menu object
			return self.menus[menuId];
		}

		// Add new menu object by menu id
		function addMenu(menuId, isPublic, roles) {
			// Create the new menu
			self.menus[menuId] = {
				isPublic: isPublic || false,
				roles: roles || self.defaultRoles,
				items: [],
				shouldRender: shouldRender
			};

			// Return the menu object
			return self.menus[menuId];
		}

		// Remove existing menu object by menu id
		function removeMenu(menuId) {
			// Validate that the menu exists
			validateMenuExistance(menuId);

			// Return the menu object
			delete self.menus[menuId];
		}

		// Add menu item object
		function addMenuItem(menuId, menuItemTitle, menuItemURL, menuItemType, menuItemUIRoute, isPublic, roles, position) {
			// Validate that the menu exists
			validateMenuExistance(menuId);

			// Push new menu item
			self.menus[menuId].items.push({
				title: menuItemTitle,
				link: menuItemURL,
				menuItemType: menuItemType || 'item',
				menuItemClass: menuItemType,
				uiRoute: menuItemUIRoute || ('/' + menuItemURL),
				isPublic: ((isPublic === null || typeof isPublic === 'undefined') ? self.menus[menuId].isPublic : isPublic),
				roles: ((roles === null || typeof roles === 'undefined') ? self.menus[menuId].roles : roles),
				position: position || 0,
				items: [],
				shouldRender: shouldRender
			});

			// Return the menu object
			return self.menus[menuId];
		}

		// Add submenu item object
		function addSubMenuItem(menuId, rootMenuItemURL, menuItemTitle, menuItemURL, menuItemUIRoute, isPublic, roles, position) {
			// Validate that the menu exists
			validateMenuExistance(menuId);

			// Search for menu item
			for (var itemIndex in self.menus[menuId].items) {
				if (self.menus[menuId].items[itemIndex].link === rootMenuItemURL) {
					// Push new submenu item
					self.menus[menuId].items[itemIndex].items.push({
						title: menuItemTitle,
						link: menuItemURL,
						uiRoute: menuItemUIRoute || ('/' + menuItemURL),
						isPublic: ((isPublic === null || typeof isPublic === 'undefined') ? self.menus[menuId].items[itemIndex].isPublic : isPublic),
						roles: ((roles === null || typeof roles === 'undefined') ? self.menus[menuId].items[itemIndex].roles : roles),
						position: position || 0,
						shouldRender: shouldRender
					});
				}
			}

			// Return the menu object
			return self.menus[menuId];
		}

		// Remove existing menu object by menu id
		function removeMenuItem(menuId, menuItemURL) {
			// Validate that the menu exists
			validateMenuExistance(menuId);

			// Search for menu item to remove
			for (var itemIndex in self.menus[menuId].items) {
				if (self.menus[menuId].items[itemIndex].link === menuItemURL) {
					self.menus[menuId].items.splice(itemIndex, 1);
				}
			}

			// Return the menu object
			return self.menus[menuId];
		}

		// Remove existing menu object by menu id
		function removeSubMenuItem(menuId, submenuItemURL) {
			// Validate that the menu exists
			validateMenuExistance(menuId);

			// Search for menu item to remove
			for (var itemIndex in self.menus[menuId].items) {
				for (var subitemIndex in self.menus[menuId].items[itemIndex].items) {
					if (self.menus[menuId].items[itemIndex].items[subitemIndex].link === submenuItemURL) {
						self.menus[menuId].items[itemIndex].items.splice(subitemIndex, 1);
					}
				}
			}

			// Return the menu object
			return self.menus[menuId];
		}

		//Adding the topbar menu
		addMenu('topbar');

		return service;
	}
})();