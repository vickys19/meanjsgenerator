(function () {

'use strict';


angular.module('core').controller('HomeController', homeController);
    
    /* @ngInject */
	function homeController(Authentication) {
		var self=this; // jshint ignore:line
		// This provides Authentication context.
		self.authentication = Authentication;
	}
})();