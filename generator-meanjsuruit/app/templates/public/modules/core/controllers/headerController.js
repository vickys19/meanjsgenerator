(function () {

'use strict';

angular.module('core').controller('HeaderController',headerController);
	/* @ngInject */
	function headerController($scope,Authentication, Menus) {
		var self=this; // jshint ignore:line

		self.authentication = Authentication;
		self.isCollapsed = false;
		self.menu = Menus.getMenu('topbar');

		self.toggleCollapsibleMenu = function() {
			self.isCollapsed = !self.isCollapsed;
		};

		// Collapsing the menu after navigation
		$scope.$on('$stateChangeSuccess', function() {
			self.isCollapsed = false;
		});
	}
})();