(function () {

'use strict';

// Setting up route
angular.module('users').config(['$stateProvider',
	function($stateProvider) {

		var headerTemplate='modules/core/views/headerTemplate.html';

		// Users state routing
		$stateProvider.
		state('profile', {
			url: '/settings/profile',
			views: {
				'header':{
					templateUrl: headerTemplate
				},
                'body': {
                    templateUrl: 'modules/users/views/settings/edit-profile.html'
                }
            }
		}).
		state('signup', {
			url: '/signup',
			views: {
				'header':{
					templateUrl: headerTemplate
				},
                'body': {
					templateUrl: 'modules/users/views/authentication/signup.html'
                }
            }
		}).
		state('signin', {
			url: '/signin',
			views: {
				'header':{
					templateUrl: headerTemplate
				},
                'body': {
					templateUrl: 'modules/users/views/authentication/signin.html'
                }
            }
		});
	}
]);

})();