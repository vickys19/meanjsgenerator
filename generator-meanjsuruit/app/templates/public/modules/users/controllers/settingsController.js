(function () {

'use strict';

angular.module('users').controller('SettingsController', settingsController);

	/* @ngInject */
	function settingsController(self, $http, $location, Users, Authentication) {
		var self=this; // jshint ignore:line

		self.user = Authentication.user;

		// If user is not signed in then redirect back home
		if (!self.user) $location.path('/');

		// Check if there are additional accounts 
		self.hasConnectedAdditionalSocialAccounts = function(provider) {
			for (var i in self.user.additionalProvidersData) {
				return true;
			}

			return false;
		};

		// Check if provider is already in use with current user
		self.isConnectedSocialAccount = function(provider) {
			return self.user.provider === provider || (self.user.additionalProvidersData && self.user.additionalProvidersData[provider]);
		};

		// Remove a user social account
		self.removeUserSocialAccount = function(provider) {
			self.success = self.error = null;

			$http.delete('/users/accounts', {
				params: {
					provider: provider
				}
			}).success(function(response) {
				// If successful show success message and clear form
				self.success = true;
				self.user = Authentication.user = response;
			}).error(function(response) {
				self.error = response.message;
			});
		};

		// Update a user profile
		self.updateUserProfile = function(isValid) {
			if (isValid) {
				self.success = self.error = null;
				var user = new Users(self.user);

				user.$update(function(response) {
					self.success = true;
					Authentication.user = response;
				}, function(response) {
					self.error = response.data.message;
				});
			} else {
				self.submitted = true;
			}
		};

		// Change user password
		self.changeUserPassword = function() {
			self.success = self.error = null;

			$http.post('/users/password', self.passwordDetails).success(function(response) {
				// If successful show success message and clear form
				self.success = true;
				self.passwordDetails = null;
			}).error(function(response) {
				self.error = response.message;
			});
		};
	}
})();