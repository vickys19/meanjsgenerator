(function () {

'use strict';

// Users service used for communicating with the users REST endpoint
angular.module('users').factory('Users',users);

	/* @ngInject */
	function users($resource) {
		return $resource('users', {}, {
			update: {
				method: 'PUT'
			}
		});
	}
})();