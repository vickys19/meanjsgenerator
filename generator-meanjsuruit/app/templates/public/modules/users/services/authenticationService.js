(function () {

'use strict';

// Authentication service for user variables
angular.module('users').factory('Authentication', [
	function() {
		var self = this;

		self._data = {
			user: window.user
		};

		return self._data;
	}
]);

})();